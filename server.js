const mongoose = require('mongoose');
const dotenv = require('dotenv');
const express=require('express');

const accountSid = 'AC693760fe1b9e87a85a988fd8094d83ee'; 
const authToken = 'ad229788c949479fc49d677107293409'; 
const client = require('twilio')(accountSid, authToken); 
 
client.messages 
      .create({ 
         body: 'kya haal hai.', 
         from: 'whatsapp:+14155238886',       
         to: 'whatsapp:+917408834898' 
       }) 
      .then(message => console.log(message.sid)) 
      .done();
// const app=express();
// app.listen(8080,(err,succ)=>{
//     console.log("Server created successfully")
// })

dotenv.config({
    path: './config.env'
});
//mongodb+srv://RahulSCJ:Rahul@123@clusterrahulscj.zm8bh.mongodb.net/SCJ_DB?retryWrites=true&w=majority
//mongodb+srv://devops:test@test@cluster0.ggzl4.mongodb.net/test

process.on('uncaughtException', err => {
    console.log('UNCAUGHT EXCEPTION!!! shutting down...');
    console.log(err);
    process.exit(1);
});

const app = require('./app');

// const database = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD);

// Connect the database
mongoose.connect(process.env.DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true 
}).then(con => {
    console.log('DB connected successfully!');
});

// Start the server
const port = process.env.PORT;
app.listen(port, () => {
    console.log(`Application is running on port ${port}`);
});

process.on('unhandledRejection', err => {
    console.log('UNHANDLED REJECTION!!!  shutting down ...');
    console.log(err.name, err.message);
    server.close(() => {
        process.exit(1);
    });
});