const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const countryModel = new Schema({
    countryCode: {
        type: String,
        required: true,
        uppercase: true,
    },
    
    countryName: {
        type: String,
        required: true,
        uppercase: true,
    },
    eeaStatus: {
        type: Boolean,
        default: false
    },
    description:{
        type:String
    },
    // createdBy: {
    //     type: Schema.Types.ObjectId,
    //     ref: "User",
    // },
    active: {
        type: Boolean,
        default: true,        
    },
    
},
    {
        timestamps: true
    });

module.exports = mongoose.model('CountryModel', countryModel);
