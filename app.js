const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
var bodyParser = require('body-parser')
const app = express();

 
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
 

const countryRoutes = require('./routes/countryRoutes');
// const productListByAdminRoutes = require('./routes/productListByAdminRoutes');
// const product = require('./routes/productLogRoutes');
// const billing = require('./routes/billingRoutes');
// const globalErrHandler = require('./controllers/errorController');
// const AppError = require('./utils/appError');


// Allow Cross-Origin requests
app.use(cors());

// Set security HTTP headers
app.use(helmet());

// Routes
 app.use('/api', countryRoutes);
 app.post('/getList',(req,res)=>{
     res.send("Anurag")
 })
// app.use('/api/v1/admin', productListByAdminRoutes);
// app.use('/api/v1/product', product);
// app.use('/api/v1/billing', billing);

// handle undefined Routes
app.use('*', (req, res, next) => {
    // const err = new AppError(404, 'fail', 'undefined route');
    // next(err, req, res, next);
    console.log("error routing")
});

// app.use(globalErrHandler);

module.exports = app;